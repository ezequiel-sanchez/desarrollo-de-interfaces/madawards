<?php

namespace app\controllers;

use Yii;
use app\models\Maillot;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * MaillotController implements the CRUD actions for Maillot model.
 */
class MaillotController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Maillot models.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Maillot::find(),
                    /*
                      'pagination' => [
                      'pageSize' => 50
                      ],
                      'sort' => [
                      'defaultOrder' => [
                      'código' => SORT_DESC,
                      ]
                      ],
                     */
            ]);

            return $this->render('index', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Maillot model.
     * @param string $código Código
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($código) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($código),
            ]);
        }
    }

    /**
     * Creates a new Maillot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = new Maillot();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'código' => $model->código]);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Maillot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $código Código
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($código) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = $this->findModel($código);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'código' => $model->código]);
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Maillot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $código Código
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($código) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $this->findModel($código)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Maillot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $código Código
     * @return Maillot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($código) {
        if (($model = Maillot::findOne(['código' => $código])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMvm() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT código FROM maillot
                             WHERE premio = (
                                SELECT MAX(premio) FROM maillot)')->queryScalar();

            $dataProvider = new ActiveDataProvider([
                'query' => Maillot::find()->where("código = '$winner'")
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Most Valued Malliot Award',
                        'award' => 'mvm',
                        'type' => 'fame',
                        'back' => 'site/maillotfame',
                        'category' => 'maillot'
            ]);
        }
    }

    public function actionMgm() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT código FROM (
                                SELECT código, COUNT(*) c FROM lleva
                                GROUP BY código) c1
                             WHERE c = (
                                    SELECT MAX(c) FROM (
                                        SELECT código, COUNT(*) c FROM lleva
                                        GROUP BY código)c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Maillot::find()->where(['in', 'código', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Most Given Maillot Award',
                        'award' => 'mgm',
                        'type' => 'shame',
                        'back' => 'site/maillotshame',
                        'category' => 'maillot'
            ]);
        }
    }

    public function actionLva() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT código FROM maillot
                             WHERE premio = (
                                SELECT MIN(premio) FROM maillot)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Maillot::find()->where(['in', 'código', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Less Valued Maillot Award',
                        'award' => 'lva',
                        'type' => 'shame',
                        'back' => 'site/maillotshame',
                        'category' => 'maillot'
            ]);
        }
    }

    public function actionMvmpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT código FROM maillot
                             WHERE premio = (
                                SELECT MAX(premio) FROM maillot)')->queryScalar();

            $dataProvider = new ActiveDataProvider([
                'query' => Maillot::find()->where("código = '$winner'")
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Most Valued Malliot Award',
                'award' => 'mvm',
                'type' => 'fame',
                'back' => 'site/maillotfame',
                'category' => 'maillot'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Most Valued Malliot Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Most Valued Malliot Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionMgmpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT código FROM (
                                SELECT código, COUNT(*) c FROM lleva
                                GROUP BY código) c1
                             WHERE c = (
                                    SELECT MAX(c) FROM (
                                        SELECT código, COUNT(*) c FROM lleva
                                        GROUP BY código)c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Maillot::find()->where(['in', 'código', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Most Given Maillot Award',
                'award' => 'mgm',
                'type' => 'shame',
                'back' => 'site/maillotshame',
                'category' => 'maillot'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Most Given Maillot Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Most Given Maillot Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionLvapdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT código FROM maillot
                             WHERE premio = (
                                SELECT MIN(premio) FROM maillot)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Maillot::find()->where(['in', 'código', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Less Valued Maillot Award',
                'award' => 'lva',
                'type' => 'shame',
                'back' => 'site/maillotshame',
                'category' => 'maillot'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Less Valued Maillot Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Less Valued Maillot Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

}
