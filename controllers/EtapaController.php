<?php

namespace app\controllers;

use Yii;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use kartik\mpdf\Pdf;

/**
 * EtapaController implements the CRUD actions for Etapa model.
 */
class EtapaController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Etapa models.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find(),
                    /*
                      'pagination' => [
                      'pageSize' => 50
                      ],
                      'sort' => [
                      'defaultOrder' => [
                      'numetapa' => SORT_DESC,
                      ]
                      ],
                     */
            ]);

            return $this->render('index', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Etapa model.
     * @param int $numetapa Numetapa
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($numetapa) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($numetapa),
            ]);
        }
    }

    /**
     * Creates a new Etapa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = new Etapa();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'numetapa' => $model->numetapa]);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Etapa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $numetapa Numetapa
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($numetapa) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = $this->findModel($numetapa);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'numetapa' => $model->numetapa]);
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Etapa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $numetapa Numetapa
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($numetapa) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $this->findModel($numetapa)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Etapa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $numetapa Numetapa
     * @return Etapa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($numetapa) {
        if (($model = Etapa::findOne(['numetapa' => $numetapa])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLsa() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM etapa
                             WHERE kms = (
                                SELECT MAX(kms) FROM etapa)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Longest Stage Award',
                        'award' => 'lsa',
                        'type' => 'fame',
                        'back' => 'site/stagefame',
                        'category' => 'etapa'
            ]);
        }
    }

    public function actionSsa() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM etapa
                             WHERE kms = (
                                SELECT MIN(kms) FROM etapa)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Shortest Stage Award',
                        'award' => 'ssa',
                        'type' => 'shame',
                        'back' => 'site/stageshame',
                        'category' => 'etapa'
            ]);
        }
    }

    public function actionMpa() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM (
                                SELECT numetapa, COUNT(*) c FROM puerto
                                GROUP BY numetapa) c1
                             WHERE c = (
                                SELECT MAX(c) FROM (
                                    SELECT numetapa, COUNT(*) c FROM puerto
                                    GROUP BY numetapa) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Most Ports Award',
                        'award' => 'mpa',
                        'type' => 'fame',
                        'back' => 'site/stagefame',
                        'category' => 'etapa'
            ]);
        }
    }

    public function actionLpa() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM (
                                SELECT numetapa, COUNT(*) c FROM puerto
                                GROUP BY numetapa) c1
                             WHERE c = (
                                SELECT MIN(c) FROM (
                                    SELECT numetapa, COUNT(*) c FROM puerto
                                    GROUP BY numetapa) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Least Ports Award',
                        'award' => 'lpa',
                        'type' => 'shame',
                        'back' => 'site/stageshame',
                        'category' => 'etapa'
            ]);
        }
    }

    public function actionTpa() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM puerto
                             WHERE altura = (
                                SELECT MAX(altura) FROM puerto)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Tallest Port Award',
                        'award' => 'tpa',
                        'type' => 'fame',
                        'back' => 'site/stagefame',
                        'category' => 'etapa'
            ]);
        }
    }

    public function actionSpa() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM puerto
                             WHERE altura = (
                                SELECT MIN(altura) FROM puerto)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner]),
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Smallest Port Award',
                        'award' => 'spa',
                        'type' => 'shame',
                        'back' => 'site/stageshame',
                        'category' => 'etapa'
            ]);
        }
    }

    public function actionLsapdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM etapa
                             WHERE kms = (
                                SELECT MAX(kms) FROM etapa)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Longest Stage Award',
                'award' => 'lsa',
                'type' => 'fame',
                'back' => 'site/stagefame',
                'category' => 'etapa'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Longest Stage Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Longest Stage Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionSsapdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM etapa
                             WHERE kms = (
                                SELECT MIN(kms) FROM etapa)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Shortest Stage Award',
                'award' => 'ssa',
                'type' => 'shame',
                'back' => 'site/stageshame',
                'category' => 'etapa'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Shortest Stage Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Shortest Stage Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionMpapdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM (
                                SELECT numetapa, COUNT(*) c FROM puerto
                                GROUP BY numetapa) c1
                             WHERE c = (
                                SELECT MAX(c) FROM (
                                    SELECT numetapa, COUNT(*) c FROM puerto
                                    GROUP BY numetapa) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Most Ports Award',
                'award' => 'mpa',
                'type' => 'fame',
                'back' => 'site/stagefame',
                'category' => 'etapa'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Most Ports Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Most Ports Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionLpapdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM (
                                SELECT numetapa, COUNT(*) c FROM puerto
                                GROUP BY numetapa) c1
                             WHERE c = (
                                SELECT MIN(c) FROM (
                                    SELECT numetapa, COUNT(*) c FROM puerto
                                    GROUP BY numetapa) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Least Ports Award',
                'award' => 'lpa',
                'type' => 'shame',
                'back' => 'site/stageshame',
                'category' => 'etapa'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Least Ports Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Least Ports Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionTpapdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM puerto
                             WHERE altura = (
                                SELECT MAX(altura) FROM puerto)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Tallest Port Award',
                'award' => 'tpa',
                'type' => 'fame',
                'back' => 'site/stagefame',
                'category' => 'etapa'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Tallest Port Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Tallest Port Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionSpapdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT numetapa FROM puerto
                             WHERE altura = (
                                SELECT MIN(altura) FROM puerto)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find()->where(['in', 'numetapa', $winner]),
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Smallest Port Award',
                'award' => 'spa',
                'type' => 'shame',
                'back' => 'site/stageshame',
                'category' => 'etapa'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Smallest Port Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Smallest Port Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

}
