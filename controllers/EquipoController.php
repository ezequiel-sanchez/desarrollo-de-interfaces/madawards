<?php

namespace app\controllers;

use Yii;
use app\models\Equipo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * EquipoController implements the CRUD actions for Equipo model.
 */
class EquipoController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Equipo models.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find(),
                    /*
                      'pagination' => [
                      'pageSize' => 50
                      ],
                      'sort' => [
                      'defaultOrder' => [
                      'nomequipo' => SORT_DESC,
                      ]
                      ],
                     */
            ]);

            return $this->render('index', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Equipo model.
     * @param string $nomequipo Nomequipo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nomequipo) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($nomequipo),
            ]);
        }
    }

    /**
     * Creates a new Equipo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = new Equipo();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'nomequipo' => $model->nomequipo]);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Equipo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nomequipo Nomequipo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nomequipo) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = $this->findModel($nomequipo);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'nomequipo' => $model->nomequipo]);
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Equipo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nomequipo Nomequipo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nomequipo) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $this->findModel($nomequipo)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Equipo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nomequipo Nomequipo
     * @return Equipo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nomequipo) {
        if (($model = Equipo::findOne(['nomequipo' => $nomequipo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMsw() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT nomequipo FROM (
                                SELECT nomequipo, SUM(v) s FROM (
                                    SELECT dorsal, COUNT(*) v FROM etapa
                                    GROUP BY dorsal) c1
                                JOIN ciclista USING (dorsal)
                                GROUP BY nomequipo) c2
                            WHERE s = (
                                SELECT MAX(s) FROM (
                                    SELECT nomequipo, SUM(v) s FROM (
                                        SELECT dorsal, COUNT(*) v FROM etapa
                                        GROUP BY dorsal) c1
                                    JOIN ciclista USING (dorsal)
                                    GROUP BY nomequipo) c2)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find()->where(['in', 'nomequipo', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Most stages Won Award',
                        'award' => 'msw',
                        'type' => 'fame',
                        'back' => 'site/teamfame',
                        'category' => 'equipo',
                        'category' => 'equipo'
            ]);
        }
    }

    public function actionLsw() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT nomequipo FROM (
                                SELECT nomequipo, SUM(v) s FROM (
                                    SELECT dorsal, COUNT(*) v FROM etapa
                                    GROUP BY dorsal) c1
                                JOIN ciclista USING (dorsal)
                                GROUP BY nomequipo) c2
                            WHERE s = (
                                SELECT MIN(s) FROM (
                                    SELECT nomequipo, SUM(v) s FROM (
                                        SELECT dorsal, COUNT(*) v FROM etapa
                                        GROUP BY dorsal) c1
                                    JOIN ciclista USING (dorsal)
                                    GROUP BY nomequipo) c2)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find()->where(['in', 'nomequipo', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Least stages Won Award',
                        'award' => 'lsw',
                        'type' => 'shame',
                        'back' => 'site/teamshame',
                        'category' => 'equipo'
            ]);
        }
    }

    public function actionBda() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT nomequipo FROM (
                                SELECT nomequipo, SUM(v) s FROM (
                                    SELECT dorsal, COUNT(*) v FROM etapa
                                    GROUP BY dorsal) c1
                                JOIN ciclista USING (dorsal)
                                GROUP BY nomequipo) c2
                            WHERE s = (
                                SELECT MAX(s) FROM (
                                    SELECT nomequipo, SUM(v) s FROM (
                                        SELECT dorsal, COUNT(*) v FROM etapa
                                        GROUP BY dorsal) c1
                                    JOIN ciclista USING (dorsal)
                                    GROUP BY nomequipo) c2)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find()->where(['in', 'nomequipo', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Best Director Award',
                        'award' => 'bda',
                        'type' => 'fame',
                        'back' => 'site/teamfame',
                        'category' => 'equipo'
            ]);
        }
    }

    public function actionWda() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT nomequipo FROM (
                                SELECT nomequipo, SUM(v) s FROM (
                                    SELECT dorsal, COUNT(*) v FROM etapa
                                    GROUP BY dorsal) c1
                                JOIN ciclista USING (dorsal)
                                GROUP BY nomequipo) c2
                            WHERE s = (
                                SELECT MIN(s) FROM (
                                    SELECT nomequipo, SUM(v) s FROM (
                                        SELECT dorsal, COUNT(*) v FROM etapa
                                        GROUP BY dorsal) c1
                                    JOIN ciclista USING (dorsal)
                                    GROUP BY nomequipo) c2)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find()->where(['in', 'nomequipo', $winner]),
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Worst Director Award',
                        'award' => 'wda',
                        'type' => 'shame',
                        'back' => 'site/teamshame',
                        'category' => 'equipo'
            ]);
        }
    }

    public function actionMswpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT nomequipo FROM (
                                SELECT nomequipo, SUM(v) s FROM (
                                    SELECT dorsal, COUNT(*) v FROM etapa
                                    GROUP BY dorsal) c1
                                JOIN ciclista USING (dorsal)
                                GROUP BY nomequipo) c2
                            WHERE s = (
                                SELECT MAX(s) FROM (
                                    SELECT nomequipo, SUM(v) s FROM (
                                        SELECT dorsal, COUNT(*) v FROM etapa
                                        GROUP BY dorsal) c1
                                    JOIN ciclista USING (dorsal)
                                    GROUP BY nomequipo) c2)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find()->where(['in', 'nomequipo', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Most stages Won Award',
                'award' => 'msw',
                'type' => 'fame',
                'back' => 'site/teamfame',
                'category' => 'equipo'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.image{width: 50px;}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Most stages Won Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Most stages Won Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionLswpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT nomequipo FROM (
                                SELECT nomequipo, SUM(v) s FROM (
                                    SELECT dorsal, COUNT(*) v FROM etapa
                                    GROUP BY dorsal) c1
                                JOIN ciclista USING (dorsal)
                                GROUP BY nomequipo) c2
                            WHERE s = (
                                SELECT MIN(s) FROM (
                                    SELECT nomequipo, SUM(v) s FROM (
                                        SELECT dorsal, COUNT(*) v FROM etapa
                                        GROUP BY dorsal) c1
                                    JOIN ciclista USING (dorsal)
                                    GROUP BY nomequipo) c2)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find()->where(['in', 'nomequipo', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Least stages Won Award',
                'award' => 'lsw',
                'type' => 'shame',
                'back' => 'site/teamshame',
                'category' => 'equipo'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Least stages Won Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Least stages Won Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionBdapdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT nomequipo FROM (
                                SELECT nomequipo, SUM(v) s FROM (
                                    SELECT dorsal, COUNT(*) v FROM etapa
                                    GROUP BY dorsal) c1
                                JOIN ciclista USING (dorsal)
                                GROUP BY nomequipo) c2
                            WHERE s = (
                                SELECT MAX(s) FROM (
                                    SELECT nomequipo, SUM(v) s FROM (
                                        SELECT dorsal, COUNT(*) v FROM etapa
                                        GROUP BY dorsal) c1
                                    JOIN ciclista USING (dorsal)
                                    GROUP BY nomequipo) c2)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find()->where(['in', 'nomequipo', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Best Director Award',
                'award' => 'bda',
                'type' => 'fame',
                'back' => 'site/teamfame',
                'category' => 'equipo'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Best Director Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Best Director Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionWdapdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT nomequipo FROM (
                                SELECT nomequipo, SUM(v) s FROM (
                                    SELECT dorsal, COUNT(*) v FROM etapa
                                    GROUP BY dorsal) c1
                                JOIN ciclista USING (dorsal)
                                GROUP BY nomequipo) c2
                            WHERE s = (
                                SELECT MIN(s) FROM (
                                    SELECT nomequipo, SUM(v) s FROM (
                                        SELECT dorsal, COUNT(*) v FROM etapa
                                        GROUP BY dorsal) c1
                                    JOIN ciclista USING (dorsal)
                                    GROUP BY nomequipo) c2)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Equipo::find()->where(['in', 'nomequipo', $winner]),
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Worst Director Award',
                'award' => 'wda',
                'type' => 'shame',
                'back' => 'site/teamshame',
                'category' => 'equipo'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Worst Director Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Worst Director Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

}
