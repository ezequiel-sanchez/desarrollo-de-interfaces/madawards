<?php

namespace app\controllers;

use Yii;
use app\models\Ciclista;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use kartik\mpdf\Pdf;

/**
 * CiclistaController implements the CRUD actions for Ciclista model.
 */
class CiclistaController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Ciclista models.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find(),
                    /*
                      'pagination' => [
                      'pageSize' => 50
                      ],
                      'sort' => [
                      'defaultOrder' => [
                      'dorsal' => SORT_DESC,
                      ]
                      ],
                     */
            ]);

            return $this->render('index', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Ciclista model.
     * @param int $dorsal Dorsal
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($dorsal) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($dorsal),
            ]);
        }
    }

    /**
     * Creates a new Ciclista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = new Ciclista();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'dorsal' => $model->dorsal]);
                }
            } else {
                $model->loadDefaultValues();
            }


            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Ciclista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $dorsal Dorsal
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($dorsal) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = $this->findModel($dorsal);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'dorsal' => $model->dorsal]);
            }


            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ciclista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $dorsal Dorsal
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($dorsal) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $this->findModel($dorsal)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Ciclista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $dorsal Dorsal
     * @return Ciclista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($dorsal) {
        if (($model = Ciclista::findOne(['dorsal' => $dorsal])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMsw() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) v FROM etapa
                                GROUP BY dorsal) c1
                             WHERE v = (
                                SELECT MAX(v) FROM (
                                   SELECT dorsal, COUNT(*) v FROM etapa
                                   GROUP BY dorsal) c1)')->queryScalar();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where("dorsal = $winner")
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Most Stages Won Award',
                        'award' => 'msw',
                        'type' => 'fame',
                        'back' => 'site/cyclistfame',
                        'category' => 'ciclista'
            ]);
        }
    }

    public function actionLsw() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) v FROM etapa
                                GROUP BY dorsal) c1
                             WHERE v = (
                                SELECT MIN(v) FROM (
                                   SELECT dorsal, COUNT(*) v FROM etapa
                                   GROUP BY dorsal) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where(['in', 'dorsal', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Least Stages Won Award',
                        'award' => 'lsw',
                        'type' => 'shame',
                        'back' => 'site/cyclistshame',
                        'category' => 'ciclista'
            ]);
        }
    }

    public function actionMmw() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) m FROM lleva
                                GROUP BY dorsal) c1
                            WHERE m = (
                                SELECT MAX(m) FROM (
                                    SELECT dorsal, COUNT(*) m FROM lleva
                                    GROUP BY dorsal) c1)')->queryScalar();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where("dorsal = $winner")
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Most Maillot Won Award',
                        'award' => 'mmw',
                        'type' => 'fame',
                        'back' => 'site/cyclistfame',
                        'category' => 'ciclista'
            ]);
        }
    }

    public function actionLmw() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) m FROM lleva
                                GROUP BY dorsal) c1
                            WHERE m = (
                                SELECT MIN(m) FROM (
                                    SELECT dorsal, COUNT(*) m FROM lleva
                                    GROUP BY dorsal) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where(['in', 'dorsal', $winner])
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Least Maillot Won Award',
                        'award' => 'lmw',
                        'type' => 'shame',
                        'back' => 'site/cyclistshame',
                        'category' => 'ciclista'
            ]);
        }
    }

    public function actionMpw() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) p FROM puerto
                                GROUP BY dorsal) c1
                             WHERE p = (
                                    SELECT MAX(p) FROM (
                                        SELECT dorsal, COUNT(*) p FROM puerto
                                        GROUP BY dorsal) c1)')->queryScalar();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where("dorsal = $winner")
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Most Ports Won Award',
                        'award' => 'mpw',
                        'type' => 'fame',
                        'back' => 'site/cyclistfame',
                        'category' => 'ciclista'
            ]);
        }
    }

    public function actionLpw() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) p FROM puerto
                                GROUP BY dorsal) c1
                             WHERE p = (
                                    SELECT MIN(p) FROM (
                                        SELECT dorsal, COUNT(*) p FROM puerto
                                        GROUP BY dorsal) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where(['in', 'dorsal', $winner]),
            ]);

            return $this->render('awards', [
                        'dp' => $dataProvider,
                        'title' => 'Least Ports Won Award',
                        'award' => 'lpw',
                        'type' => 'shame',
                        'back' => 'site/cyclistshame',
                        'category' => 'ciclista'
            ]);
        }
    }

    public function actionMswpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) v FROM etapa
                                GROUP BY dorsal) c1
                             WHERE v = (
                                SELECT MAX(v) FROM (
                                   SELECT dorsal, COUNT(*) v FROM etapa
                                   GROUP BY dorsal) c1)')->queryScalar();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where("dorsal = $winner")
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Most Stages Won Award',
                'award' => 'msw',
                'type' => 'fame',
                'back' => 'site/cyclistfame',
                'category' => 'ciclista'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Most Stages Won Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Most Stages Won Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionLswpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) v FROM etapa
                                GROUP BY dorsal) c1
                             WHERE v = (
                                SELECT MIN(v) FROM (
                                   SELECT dorsal, COUNT(*) v FROM etapa
                                   GROUP BY dorsal) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where(['in', 'dorsal', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Least Stages Won Award',
                'award' => 'lsw',
                'type' => 'shame',
                'back' => 'site/cyclistshame',
                'category' => 'ciclista'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Least Stages Won Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Least Stages Won Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionMmwpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) m FROM lleva
                                GROUP BY dorsal) c1
                            WHERE m = (
                                SELECT MAX(m) FROM (
                                    SELECT dorsal, COUNT(*) m FROM lleva
                                    GROUP BY dorsal) c1)')->queryScalar();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where("dorsal = $winner")
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Most Maillot Won Award',
                'award' => 'mmw',
                'type' => 'fame',
                'back' => 'site/cyclistfame',
                'category' => 'ciclista'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Most Maillot Won Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Most Maillot Won Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionLmwpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) m FROM lleva
                                GROUP BY dorsal) c1
                            WHERE m = (
                                SELECT MIN(m) FROM (
                                    SELECT dorsal, COUNT(*) m FROM lleva
                                    GROUP BY dorsal) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where(['in', 'dorsal', $winner])
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Least Maillot Won Award',
                'award' => 'lmw',
                'type' => 'shame',
                'back' => 'site/cyclistshame',
                'category' => 'ciclista'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Least Maillot Won Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Least Maillot Won Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionMpwpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) p FROM puerto
                                GROUP BY dorsal) c1
                             WHERE p = (
                                    SELECT MAX(p) FROM (
                                        SELECT dorsal, COUNT(*) p FROM puerto
                                        GROUP BY dorsal) c1)')->queryScalar();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where("dorsal = $winner")
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Most Ports Won Award',
                'award' => 'mpw',
                'type' => 'fame',
                'back' => 'site/cyclistfame',
                'category' => 'ciclista'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Most Ports Won Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Most Ports Won Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

    public function actionLpwpdf() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $winner = Yii::$app->db->createCommand(
                            'SELECT DISTINCT dorsal FROM (
                                SELECT dorsal, COUNT(*) p FROM puerto
                                GROUP BY dorsal) c1
                             WHERE p = (
                                    SELECT MIN(p) FROM (
                                        SELECT dorsal, COUNT(*) p FROM puerto
                                        GROUP BY dorsal) c1)')->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Ciclista::find()->where(['in', 'dorsal', $winner]),
            ]);

            $content = $this->renderPartial('awards', [
                'dp' => $dataProvider,
                'title' => 'Least Ports Won Award',
                'award' => 'lpw',
                'type' => 'shame',
                'back' => 'site/cyclistshame',
                'category' => 'ciclista'
            ]);

            $pdf = new Pdf([
                // set to use core fonts only
                'mode' => Pdf::MODE_CORE,
                // A4 paper format
                'format' => Pdf::FORMAT_A4,
                // portrait orientation
                'orientation' => Pdf::ORIENT_PORTRAIT,
                // stream to browser inline
                'destination' => Pdf::DEST_BROWSER,
                // your html content input
                'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:18px}',
                // set mPDF properties on the fly
                'options' => ['title' => 'Least Ports Won Award'],
                // call mPDF methods on the fly
                'methods' => [
                    'SetHeader' => ['Least Ports Won Award'],
                    'SetFooter' => ['{PAGENO}'],
                ]
            ]);
            return $pdf->render();
        }
    }

}
