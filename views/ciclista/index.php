<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cyclists';
$this->params['breadcrumbs'][] = 'Cyclists';
?>
<div class="ciclista-index">

    <h1>Cyclists</h1>

    <p>
        <?= Html::a('Create Cyclist', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'dorsal',
            'nombre',
            'edad',
            'nomequipo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'dorsal' => $model->dorsal]);
                 }
            ],
        ],
    ]); ?>


</div>
