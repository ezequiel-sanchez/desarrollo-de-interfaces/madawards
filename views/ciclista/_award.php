<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$dorsal = $model->dorsal;
?>

<div class="jumbotron bg-transparent text-white <?= $type ?>-card d-flex wrap">
    <div class="col-12"><h1 class="text-center display-4 wrap"><?= "" . $model->nombre ?></h1></div>
    <div class="col-3">

        <?= Html::img("@web/images/$dorsal.png", ['class' => 'resize']) ?></div>

    <div class="col-5 align-middle">    
        <h2><?= "Age: " . $model->edad ?></h2>
        <h2><?= "Dorsal: " . $model->dorsal ?></h2>
        <h2><?= "Team: " . $model->nomequipo ?></h2>
    </div>

    <div class="col-4 d-flex wrap">

        <?php
        if ($award === "msw" or $award === "lsw") {
            echo "<div class='col-12'><h3 class='text-center'>Stages Won</h3></div><div class = 'col-6'><h4 class='text-center'>Nº</h4><div class='text-center'>",
            implode('</div><div class="text-center">', ArrayHelper::getColumn($model->etapas, 'numetapa'));
            echo "</div></div><div class='col-6'><h4 class='text-center'>Location</h4><div class='text-center'>",
            implode('</div><div class="text-center">', ArrayHelper::getColumn($model->etapas, 'salida'));
            echo "</div>";
        }

        if ($award === "mmw" or $award === "lmw") {
            echo "<div class='col-12'><h3>Maillots Won</h3></div><div class = 'col-6'><div>",
            implode('</div><div>', ArrayHelper::getColumn($model->llevas, 'código'));
            echo "</div>";
        }

        if ($award === "mpw" or $award === "lpw") {
            echo "<div class='col-12'><h3>Ports Won</h3></div><div class = 'col-12'><div>",
            implode('</div><div>', ArrayHelper::getColumn($model->puertos, 'nompuerto'));
            echo "</div>";
        }
        ?>
    </div>

</div>

<div>
    <p>
</div>