<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$numetapa = $model->numetapa;
?>

<div class="jumbotron bg-transparent text-white <?= $type ?>-card d-flex wrap">
    <div class="col-12"><h1 class="text-center display-4 wrap"><?= "Stage #" . $model->numetapa ?></h1></div>
    <div class="col-3">

        <?= Html::img("@web/images/e_$numetapa.png", ['class' => 'resize']) ?></div>

    <div class="col-5 align-middle">    
        <h4><?= "Longitude: " . $model->kms ?>kms</h4>
        <h4><?= "Start location: " . $model->salida ?></h4>
        <h4><?= "Finish location: " . $model->llegada ?></h4>
    </div>

    <div class="col-4 d-flex wrap">

        <?php
        echo "<div class='col-12'><h2 class='text-center'>Ports</h2></div><div class = 'col-8'><h4 class='text-center'>Port</h4><div class='text-center'>",
        implode('</div><div class="text-center">', ArrayHelper::getColumn($model->puertos, 'nompuerto'));
        echo "</div></div><div class='col-4'><h4 class='text-center'>Height</h4><div class='text-center'>",
        implode('</div><div class="text-center">', ArrayHelper::getColumn($model->puertos, 'altura'));
        echo "</div>";
        ?>
    </div>

</div>

<div>
    <p>
</div>