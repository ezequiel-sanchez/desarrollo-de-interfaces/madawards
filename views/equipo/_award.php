<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$director = $model->director;
$nomequipo = $model->nomequipo;

if ($award === 'msw'or $award === 'lsw') {
    echo
    "<div class='jumbotron bg-transparent text-white ", "$type", "-card d-flex wrap'>
    <div class='col-12'><h1 class='text-center display-4 wrap'>Team: ", "$nomequipo", "</h1></div>
    
    <div class='col-3'>
        ", Html::img("@web/images/$nomequipo.png", ['class' => 'resize']), "
    </div>

    <div class='col-8 align-middle text-center'>
        <h2>Director: ", "$director", "</h2>
    </div>";
}

if ($award === 'bda'or $award === 'wda') { {
        echo
        "<div class='jumbotron bg-transparent text-white ", "$type", "-card d-flex wrap'>
    <div class='col-12'><h1 class='text-center display-4 wrap'>", "$director", "</h1></div>
    
    <div class='col-3'>
        ", Html::img("@web/images/$director.png", ['class' => 'resize']), "
    </div>

    <div class='col-8 align-middle text-center'>
        <h2>Team: ", "$nomequipo", "</h2>
    </div>";
    }
}
?>

<div>
    <p>
</div>