<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;
use yii\helpers\ArrayHelper;

if ($type === 'shame') {
    $font = 'comic';
} else {
    $font = '';
}
$print = "$category/$award" . "pdf";
?>

<div class="col-1">
    <?= Html::a(Html::img("@web/images/back.png", ['class' => 'resize']), [$back]) ?>
</div>
<div class="well well-sm " id="toplink">
    <h1 class="display-3 text-center <?= $font ?>"><?= $title ?></h1>
</div>

<div class="row wrap">
    <?=
    ListView::widget([
        'dataProvider' => $dp,
        'itemView' => '_award',
        'layout' => "{summary}\n{pager}\n{items}",
        'viewParams' => ['award' => "$award", 'type' => "$type"],
    ]);
    ?>

</div>

<?php
echo "<div class='text-left'>", Html::a('Print dossier', [$print], ['class' => 'btn btn-primary']), "</div>";
if ($type === 'fame') {
    echo "<div class='text-right'>", Html::a('Go Back', [$back], ['class' => 'btn btn-warning']), "</div>";
}

if ($type === 'shame') {
    echo "<div class='text-right'>", Html::a('Go Back', [$back], ['class' => 'btn btn-danger']), "</div>";
}
?>