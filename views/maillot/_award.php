<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$code = $model->código;
?>

<div class="jumbotron bg-transparent text-white <?= $type ?>-card d-flex wrap">
    <div class="col-12"><h1 class="text-center display-4 wrap"><?= "Maillot Code: " . $model->código ?></h1></div>
    <div class="col-3">

        <?= Html::img("@web/images/$code.png", ['class' => 'resize']) ?></div>

    <div class="col-9 align-middle">    
        <h2><?= "Type: " . $model->tipo ?></h2>
        <h2><?= "Color: " . $model->color ?></h2>
        <h2><?= "Prize: € " . $model->premio ?></h2>
    </div>

</div>

<div>
    <p>
</div>